Here is a [short video](https://vimeo.com/376471295) showing what this patch does.

Learn more about this project in this [paper](https://git.forum.ircam.fr/MikeLi/spatialized-additive-resynthesizer/blob/master/Spatialized_real-time_additive_resynthesis_in_Max_MSP_using_Spat_and_Sigmund.pdf)(not very long) and the [poster](https://git.forum.ircam.fr/MikeLi/spatialized-additive-resynthesizer/blob/master/Spatialized_real-time_additive_resynthesis_in_Max_MSP_using_Spat_and_Sigmund_poster_.pdf).

The basic instructions can be found in the Max presentation mode.

[Spat5](https://forum.ircam.fr/projects/detail/spat/) and [Odot](https://cnmat.berkeley.edu/downloads) externals are required.




Thanks to Jeffrey Lubow and Dr. John MacCallum for sharing the Spec_as_Prob patch.
